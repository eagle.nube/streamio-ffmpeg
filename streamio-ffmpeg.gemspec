# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib/', __FILE__)
$:.unshift lib unless $:.include?(lib)

require "ffmpeg/version"

Gem::Specification.new do |s|
  s.name        = "streamio-ffmpeg"
  s.version     = FFMPEG::VERSION
  s.authors     = ["Eaglenube"]
  s.email       = ["admin@eaglenube.com", "eaglenube.com"]
  s.homepage    = "http://gitlab.com/eagle.nube/streamio-ffmpeg"
  s.summary     = "Transcodes videos with ffmpeg"

  s.add_dependency('multi_json', '~> 1.8')

  s.add_development_dependency("rspec", "~> 3")
  s.add_development_dependency("rake", "~> 10.1")

  s.files        = Dir.glob("lib/**/*") + %w(README.md LICENSE CHANGELOG)
end
